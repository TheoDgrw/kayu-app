import { useEffect, useState } from "react";
import { getString } from "../functions/asyncStorage";


export default function useToken() {
    const [token, setToken] = useState<string | null>();

    useEffect(() => {
        (async function() {
            const token = await getString('jwt');
            token ? setToken(token) : setToken(null);
        });
    }, []);

    return token;
}