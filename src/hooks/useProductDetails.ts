import { FOOD_API_URL } from "@env";
import axios, { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { ProductDto } from "../interfaces/ProductDto";


export default function useProductDetails(barCode: string): null | ProductDto {
    const [infos, setInfos] = useState<null | ProductDto>(null);

    useEffect(() => {
        (async () => {
            let response: AxiosResponse;
            try {
                response = await axios.get(`${FOOD_API_URL}/product/${barCode}.json`);
                console.log(response.data);
                setInfos(response.data);
            } catch(e) {
                setInfos(null);
            }
        })();
    }, []);

    return infos;
}