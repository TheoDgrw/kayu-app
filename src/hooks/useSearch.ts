import { FOOD_SEARCH_URL } from "@env";
import axios from "axios";
import { useEffect, useState } from "react";
import { IProduct } from "../interfaces/IProduct";
import { IngredientDto, ProductDto } from "../interfaces/ProductDto";

export default function useSearch(search: string) {
    const [results, setResults] = useState<IProduct[] | null>(null);

    useEffect(() => {
        if (search) {
            (async () => {
                try {
                    const response = await axios.get(`${FOOD_SEARCH_URL}?search_terms=${search}&search_simple=1&action=process&json=1`);
                    setResults(response.data.products);
                } catch(e) {
                    console.error(e);
                }
            })();
        } else {
            setResults([]);
        }
    }, [search]);

    return results;
}