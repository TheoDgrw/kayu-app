import AsyncStorage from "@react-native-async-storage/async-storage";

export const storeString = async (storageKey: string, value: string) => {
    try {
        await AsyncStorage.setItem(storageKey, value)
    } catch (e) {
        console.error(e);
    }
}

export const storeData = async (storageKey: string, value: {}) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(storageKey, jsonValue)
    } catch (e) {
        console.error(e);
    }
}

export const getString = async (storageKey: string) => {
    try {
        const value = await AsyncStorage.getItem(storageKey)
        if (value !== null) {

            return value;
        }
    } catch(e) {
        console.error(e);
    }
}

export const getData = async (storageKey: string) => {
    try {
        const jsonValue = await AsyncStorage.getItem(storageKey);

        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
        console.error(e);
    }
}