import React, { useContext, useState } from "react";
import { StyleSheet, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import CustomBtn from "../components/CustomBtn";
import Main from "../layouts/Main";
import colors from "../styles/colors";
import { AuthContext } from "../utils/context";

export default function SignUpScreen() {
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const { signUp } = useContext(AuthContext);

    const handleEmail = (text: string) => {
        setEmail(text);
    }

    const handleUsername = (text: string) => {
        setUsername(text);
    }

    const handlePassword = (text: string) => {
        setPassword(text);
    }

    return (
        <Main>
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    value={email}
                    onChangeText={handleEmail}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Username"
                    value={username}
                    onChangeText={handleUsername}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={password}
                    onChangeText={handlePassword}
                />
            </View>
            <CustomBtn text="Sign Up" callback={() => signUp({ email, username, password })} />
        </Main>
    );
}

const styles = StyleSheet.create({
    container: {
        height: 120,
        justifyContent: 'space-around'
    },
    input: {
        borderWidth: 1,
        borderColor: '#000',
        backgroundColor: '#FFF',
        paddingHorizontal: 30,
        textAlign: 'center'
    },
});