export enum screens {
    HomeMenuScreen = 'HomeMenu',
    ProductDetailsScreen = 'ProductDetails',
    Scanner = 'Scanner',
    SignInScreen = 'SignIn',
    SignUpScreen = 'SignUp',
    SplashScreen = 'Splash',
    SearchScreen = 'Search'
}