import { useNavigation } from "@react-navigation/native";
import React, { useContext, useState } from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { AuthContext } from "../utils/context";
import colors from "../styles/colors";
import CustomBtn from "../components/CustomBtn";
import SplashScreen from "./SplashScreen";
import Main from "../layouts/Main";

export default function SignInScreen() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const { navigate } = useNavigation();

    const { signIn } = useContext(AuthContext);

    const signInCallback = () => {
        setIsLoading(true);
        signIn({ username, password });
    }

    const handleUsername = (text: string) => {
        setUsername(text);
    }

    const handlePassword = (text: string) => {
        setPassword(text);
    }

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <Main>
            <View style={styles.container}>
                <TextInput
                    autoCapitalize="none"
                    style={styles.input}
                    placeholder="username"
                    value={username}
                    onChangeText={handleUsername}
                />
                <TextInput
                    autoCapitalize="none"
                    style={styles.input}
                    placeholder="password"
                    value={password}
                    onChangeText={handlePassword}
                    secureTextEntry={true}
                />
            </View>
            <CustomBtn callback={signInCallback} text="Sign In" />
            <CustomBtn callback={() => navigate('SignUp')} text="Sign Up" />
        </Main>
    );
}

const styles = StyleSheet.create({
    container: {
        height: 90,
        justifyContent: 'space-around'
    },
    input: {
        borderWidth: 1,
        borderColor: '#000',
        backgroundColor: '#FFF',
        paddingHorizontal: 30,
        textAlign: 'center'
    },
});