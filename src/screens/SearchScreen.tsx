import React, { useState } from "react";
import { TextInput } from "react-native-gesture-handler";
import useSearch from "../hooks/useSearch";
import Main from "../layouts/Main";
import AutoComplete from "../components/AutoComplete";
import { StyleSheet, Text, View } from "react-native";
import { AntDesign } from '@expo/vector-icons';
import colors from "../styles/colors";


export default function SearchScreen() {
    const [search, setSearch] = useState('');

    const results = useSearch(search);

    return(
        <Main>
            <View style={styles.container}>
                <View style={styles.searchBloc}>
                    <TextInput style={styles.searchInput} value={search} placeholder="Search" onChangeText={(text: string) => setSearch(text)} />
                    <AntDesign style={styles.icon} name="search1" size={32} color="black" />
                </View>
                {
                    (results && results.length > 0) ?
                        <AutoComplete choices={results} />
                        : <Text>Search for a product</Text>
                }
            </View>
        </Main>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 50,
        justifyContent: 'center'
    },
    searchBloc: {
        top: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 260
    },
    icon: {
        padding: 7,
        borderRadius: 100,
        borderColor: colors.orange,
        borderWidth: 2,
        backgroundColor: '#FFF',
        color: colors.orange
    },
    searchInput: {
        backgroundColor: '#FFF',
        height: 50,
        width: 200,
        alignSelf: 'center',
        borderRadius: 25,
        paddingHorizontal: 25,
        borderWidth: 2,
        borderColor: colors.orange
    }
})