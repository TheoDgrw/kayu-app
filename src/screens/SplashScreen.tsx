import React from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import Main from "../layouts/Main";
import colors from "../styles/colors";

export default function SplashScreen() {

    return (
        <Main>
            <View style={[styles.horizontal, styles.container]}>
                <ActivityIndicator size="large" color={colors.darkPurple} />
            </View>
        </Main>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: colors.yellow
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});