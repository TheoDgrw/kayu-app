import { RouteProp, useRoute } from "@react-navigation/core";
import React from "react";
import { FlatList, FlatListProps, Image, ListRenderItem, StyleSheet, Text, View } from "react-native";
import IngredientDetails from "../components/ingredient/IngredientDetails";
import useProductDetails from "../hooks/useProductDetails";
import { IngredientDto } from "../interfaces/ProductDto";
import Main from "../layouts/Main";
import { ScannerStackParamsList } from "../stacks/scanner/ScannerStack";
import Splash from "./SplashScreen";

export default function ProductDetailsScreen() {

    const route = useRoute<RouteProp<ScannerStackParamsList, 'ProductDetails'>>();
    const barCode = route.params.barCode;
    const details = useProductDetails(barCode);

    const renderIngredient: ListRenderItem<IngredientDto> = ( { item }) => {
        return (
            <IngredientDetails ingredient={item} />
        );
    }

    if (details === null) {

        return <Splash />
    }

    return (
        <Main>
            <Image style={styles.image} source={{ uri: details.product.image_small_url }} />
            <Text>{details.product.product_name}</Text>
            <FlatList style={styles.ingredient} keyExtractor={item => item.id} data={details.product.ingredients} renderItem={renderIngredient} />
        </Main>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: 200,
        height: 200,
        borderRadius: 100
    },
    ingredient: {
        backgroundColor: '#FFF',
        alignSelf: 'stretch'
    }
})