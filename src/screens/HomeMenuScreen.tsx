import { useNavigation } from "@react-navigation/core";
import React from "react";
import { StyleSheet, View } from "react-native";
import CustomBtn from "../components/CustomBtn";
import { stacks } from "../stacks/types/stacksEnum";
import colors from "../styles/colors";
import { screens } from "./types/screensEnum";

/* TODO: remplacer jsp */

export default function HomeMenuScreen() {

    const { navigate } = useNavigation();

    return (
        <View style={styles.container}>
            <CustomBtn text="Scanner" callback={() => navigate(screens.Scanner)} />
            <CustomBtn text="Search" callback={() => navigate(screens.SearchScreen)} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.yellow
    }
});