
const colors = {
    yellow: '#FFC300',
    orange: '#FF5733',
    red: '#C70039',
    purple: '#900C3F',
    darkPurple: '#581845'
}

export default colors;