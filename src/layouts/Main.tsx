import React, { PropsWithChildren } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import colors from "../styles/colors";


export default function Main({ children }: PropsWithChildren<{}>) {

    return (
        <SafeAreaView style={styles.container}>
            {children}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.yellow
    }
});