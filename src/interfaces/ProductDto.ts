export type IngredientDto = {
    id: string;
    percent_estimate: number;
    percent_max: number;
    percent_min: number;
    rank: number;
    text: string;
    vegetarian: "yes" | "no";
    vegan: "yes" | "no";
}

export type ProductDto = {
    code: string;
    status: number;
    status_verbose: string;
    product: {
        image_small_url: string
        image_thumb_url: string
        image_url: string
        ingredients: Array<IngredientDto>
        ingredients_text: string
        nutriscore_grade: String
        nutriscore_score: number
        nutriscore_data: {score: string}
        origins: string
        product_name: string
        quantity: string
    }
}