import { createDrawerNavigator } from "@react-navigation/drawer";
import React from "react";
import ProductDetailsScreen from "../../screens/ProductDetailsScreen";
import SearchScreen from "../../screens/SearchScreen";
import { screens } from "../../screens/types/screensEnum";


export type SearchStackParamsList = {
    [screens.SearchScreen]: undefined;
    [screens.ProductDetailsScreen]: { barCode: string };
}

export default function SearchStack() {

    const { Navigator, Screen } = createDrawerNavigator<SearchStackParamsList>();

    return(
        <>
            <Navigator>
                <Screen name={screens.SearchScreen} component={SearchScreen} />
                <Screen name={screens.ProductDetailsScreen} component={ProductDetailsScreen} />
            </Navigator>
        </>
    );
}