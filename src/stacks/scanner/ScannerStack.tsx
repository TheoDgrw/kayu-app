import { createDrawerNavigator } from "@react-navigation/drawer";
import React from "react";
import ProductDetailsScreen from "../../screens/ProductDetailsScreen";
import ScannerScreen from "../../screens/ScannerScreen";


export type ScannerStackParamsList = {
    Scanner: undefined;
    ProductDetails: { barCode: string };
}

export default function ScannerStack() {

    const { Navigator, Screen } = createDrawerNavigator<ScannerStackParamsList>();

    return (
        <>
            <Navigator>
                <Screen name="Scanner" component={ScannerScreen} />
                <Screen name="ProductDetails" component={ProductDetailsScreen} />
            </Navigator>
        </>
    );
}