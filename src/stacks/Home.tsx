import { useNavigation } from "@react-navigation/core";
import { createDrawerNavigator } from "@react-navigation/drawer";
import React from "react";
import { StyleSheet } from "react-native";
import colors from "../styles/colors";
import HomeMenuScreen from "../screens/HomeMenuScreen";
import ScannerScreen from "../screens/ScannerScreen";
import ProductDetailsScreen from "../screens/ProductDetailsScreen";
import ScannerStack from "./scanner/ScannerStack";
import SearchStack from "./search/SearchStack";


export type HomeStackParamsList = {
    HomeMenu: undefined;
    Scanner: undefined;
    Search: undefined;
}

export default function Home() {

    const { Navigator, Screen } = createDrawerNavigator<HomeStackParamsList>();

    const { navigate } = useNavigation();

    return (
        <>
            <Navigator>
                <Screen name="HomeMenu" component={HomeMenuScreen} />
                <Screen name="Scanner" component={ScannerStack} />
                <Screen name="Search" component={SearchStack} />
            </Navigator>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.yellow
    },
    button: {
        borderColor: '#000',
        borderStyle: 'solid',
        borderWidth: 2,
        borderRadius: 3,
        paddingHorizontal: 25,
        paddingVertical: 10,
        backgroundColor: '#FF5733'
    },
    buttonText: {
        color: '#FFF'
    }
});