import { createContext } from "react";
import UserData from "../interfaces/UserData";


interface ProviderValue {
    signIn: ({ username, password }: UserData) => Promise<void>;
    signOut: () => void;
    signUp: ({ email, username, password }: UserData) => Promise<void>;
  }

export const AuthContext = createContext<ProviderValue>({} as ProviderValue);