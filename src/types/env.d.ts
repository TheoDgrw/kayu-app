declare module '@env' {
    export const API_URL: string;
    export const FOOD_API_URL: string;
    export const FOOD_SEARCH_URL: string;
}