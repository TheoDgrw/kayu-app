import React from "react";
import { StyleSheet, View } from "react-native";
import { IngredientDto } from "../../interfaces/ProductDto";
import colors from "../../styles/colors";
import IngredientLine from "./IngredientLine";


export default function IngredientDetails({ ingredient }: { ingredient: IngredientDto }) {

    return (
        <View style={styles.container}>
            <IngredientLine title="Rank:" ingredientInfo={ingredient.rank} />
            <IngredientLine title="Percentage:" ingredientInfo={Math.round(ingredient.percent_estimate)} />
            <IngredientLine title="Description:" ingredientInfo={ingredient.text} />
            <View style={styles.separator}></View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {

    },
    separator: {
        height: 2,
        backgroundColor: colors.darkPurple,
        marginHorizontal: 40,
        marginVertical: 20
    }
});