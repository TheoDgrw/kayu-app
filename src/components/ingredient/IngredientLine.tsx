import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { IngredientDto } from "../../interfaces/ProductDto";
import colors from "../../styles/colors";

export default function IngredientLine({ ingredientInfo, title }: { ingredientInfo: string | number, title: string }) {

    return (
        <View style={styles.container}>
            <View style={styles.titleBloc}>
                <Text style={styles.title}>{title}</Text>
                <View style={styles.triangle}></View>
            </View>
            <View style={styles.titleBloc}>
                <View style={styles.triangleDesc}></View>
                <Text style={styles.description}>{ingredientInfo}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginVertical: 5
    },
    title: {
        textAlign: 'center',
        backgroundColor: colors.yellow,
        color: '#FFF',
        paddingHorizontal: 10,
        borderWidth: 2,
        borderRadius: 2,
        borderLeftColor: colors.orange,
        borderTopColor: colors.orange,
        borderBottomColor: colors.orange,
        borderRightColor: colors.yellow,
        zIndex: 1
    },
    titleBloc : {
        flexDirection: 'row'
    },
    description: {
        textAlign: 'center',
        backgroundColor: colors.yellow,
        color: '#FFF',
        paddingHorizontal: 10,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: colors.orange,
        zIndex: -2
    },
    triangle: {
        aspectRatio: 1,
        zIndex: 0,
        transform: [
            { rotate: '45deg' }
        ],
        borderWidth: 2,
        borderRadius: 2,
        borderColor: colors.orange,
        backgroundColor: colors.yellow,
        left: -10
    },
    triangleDesc: {
        aspectRatio: 1,
        zIndex: 2,
        transform: [
            { rotate: '45deg' }
        ],
        borderRadius: 2,
        right: -10,
    }
});