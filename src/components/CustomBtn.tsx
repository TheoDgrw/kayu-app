import React from "react";
import { GestureResponderEvent, Pressable, StyleSheet, Text } from "react-native";
import colors from "../styles/colors";

interface Props {
    callback: (event: GestureResponderEvent) => void,
    text: string
}

export default function CustomBtn({ callback, text }: Props) {

    return (
        <Pressable style={styles.button} onPress={callback}>
            <Text style={styles.text}>{text}</Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    button: {
        marginTop: 20,
        borderColor: '#000',
        borderStyle: 'solid',
        borderWidth: 2,
        borderRadius: 3,
        paddingHorizontal: 25,
        paddingVertical: 10,
        backgroundColor: colors.orange
    },
    text: {
        color: '#FFF',
    }
});