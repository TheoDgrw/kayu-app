import React from "react";
import { ListRenderItem, ScrollView, StyleSheet, Text, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { IProduct } from "../interfaces/IProduct";
import { IngredientDto, ProductDto } from "../interfaces/ProductDto";

interface Props {
    choices: IProduct[];
}

export default function AutoComplete({ choices }: Props) {

    const renderChoice: ListRenderItem<IProduct> = ({ item }) => {

        return <Text>{ item.product_name }</Text>
    }

    return (
        <FlatList style={styles.list} data={choices} renderItem={renderChoice} />
    );
}

const styles = StyleSheet.create({
    list: {
        maxHeight: 50,
        alignSelf: 'center'
    }
})