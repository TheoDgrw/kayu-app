import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import React, { Reducer, useEffect, useMemo, useReducer } from 'react';
import Home from './src/stacks/Home';
import Splash from './src/screens/SplashScreen';
import { getItemAsync } from 'expo-secure-store';
import axios from 'axios';
import { API_URL } from '@env';
import UserData from './src/interfaces/UserData';
import { AuthContext } from './src/utils/context';
import SignInScreen from './src/screens/SignInScreen';
import SignUpScreen from './src/screens/SignUpScreen';

export type RootStackParamList = {
  SignIn: undefined;
  SignUp: undefined;
  Home: undefined;
};

interface Action {
  type: string;
  token: string | null;
}

interface State {
  isLoading?: boolean;
  isSignOut?: boolean;
  userToken?: string | null;
}

interface ResponseDto {
  status?: string;
  message?: string;
  error?: string;
}

interface UserDto extends ResponseDto {
  username: string;
  email: string;
  token: string;
}

const { Navigator, Screen } = createDrawerNavigator<RootStackParamList>();

export default function App() {
  const [state, dispatch] = useReducer<Reducer<State | undefined, Action>>(
    (prevState, action) => {
      switch(action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            isLoading: false,
            userToken: action.token
          }
        case 'AWAITING':
          return {
            ...prevState,
            isLoading: true
          }
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignOut: false,
            userToken: action.token
          }
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignOut: true,
            userToken: null
          }
      }
    },
    {
      isLoading: true,
      isSignOut: false,
      userToken: null
    }
  )
  // const [isSignOut, setIsSignOut] = useState(false);
  // const [isLoading, setIsLoading] = useState(true);

  // const token = useToken();

  useEffect(() => {
    const bootstrapAsync =  async () => {
      let userToken: string | null;

      try {
        userToken = await getItemAsync('userToken');
      } catch(e) {
        userToken = null;
        console.error('restoring token failed'); // TODO: display a message telling user auth failed
      }

      // TODO: check if token is still valid and store its expiration date (also a refresh token could be good)

      dispatch({ type:  'RESTORE_TOKEN', token: userToken});
    }
    bootstrapAsync();
  }, []);

  const authContext = useMemo(() => ({
    signIn: async ({username, password}: UserData) => {
      let userToken: string = '';

      try {
        const response = await axios.post(`${API_URL}/user/auth`, { user: { username, password } }); // TODO: remplacer les ????
        const userInfos: UserDto = response.data;
        
        dispatch({ type: 'SIGN_IN', token: userInfos.token });
      } catch(e) {
        console.error(e);
      }
    },
    signOut: () => dispatch({ type: 'SIGN_OUT', token: null }),
    signUp: async ({ email, username, password }: UserData) => {
      const data: {token: string} = await axios.post(`${API_URL}/user/create`, { user: { email, username, password } });

      dispatch({ type: 'SIGN_IN', token: data.token });
    }
  }), []);

  if (state?.isLoading) {
    return <Splash />
  }

  return (
    <NavigationContainer>
      <AuthContext.Provider value={authContext}>
        <Navigator>
          {state?.userToken === null ? (
            <>
              <Screen
                name='SignIn'
                component={SignInScreen}
                options={{
                  title: 'Sign in',
                }}
              />
              <Screen
                name='SignUp'
                component={SignUpScreen}
                options={{
                  title: 'Sign up'
                }}
              />
            </>
          ) : (
            <>
              <Screen name='Home' component={Home} />
            </>
          )}
        </Navigator>
      </AuthContext.Provider>
    </NavigationContainer>
  );
}